package movies.importer;

import java.util.ArrayList;

public class LowercaseProcessor extends Processor {

	public LowercaseProcessor(String source, String destination) {
		super(source, destination, true);
	}

	public ArrayList<String> process(ArrayList<String> lower) {
		ArrayList<String> asLower = new ArrayList<String>();
		for (int i = 0; i < lower.size(); i++) {
			asLower.add(lower.get(i).toLowerCase());
		}
		return asLower;
	}
}
