package movies.importer;

import java.util.*;

public class RemoveDuplicates extends Processor {
	public RemoveDuplicates(String source, String destination) {
		super(source, destination, false);

	}

	public ArrayList<String> process(ArrayList<String> duplicate) {
		ArrayList<String> noDuplicates = new ArrayList<String>();
		for (int i = 0; i < duplicate.size(); i++) {
			if (!noDuplicates.contains(duplicate.get(i))) {
				noDuplicates.add(duplicate.get(i));
			}
		}
		return noDuplicates;
	}
}
